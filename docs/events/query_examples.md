Inspect the event table
```sql
SELECT *
FROM events.all_events
```

Select an event by name:
```sql
SELECT *
FROM events.all_events
WHERE name = 'GW170817'
```

Select an event by GPS time:
```sql
SELECT *
FROM events.all_events
WHERE ABS(geocent_time_gps - 1187008882) < 1
```
