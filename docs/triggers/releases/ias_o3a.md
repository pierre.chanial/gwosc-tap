# IAS-O3a

## Preprocessing

The data is read from a pandas-written hdf file. The columns `name`, `obs` and `trig` have an object data type, although no None values are present. Their data type is converted to the expected type.
The `ifar` column has one entry equal to zero and many equal to +infinity. These values are kept.


## Standardization

| Standard field  | Derivation from original fields |
|-----------------|-----------------------|
| `name`          | 'IAS-O3a-' + `name`   |
| `original_name` | `name`                |
| `release`       | 'IAS-O3a'             |
| `catalog`       | 'IAS-O3a'             |
| `pipeline`      | 'IAS'                 |
| `network`       | `obs` ('HLV' → 'H1,L1,V1') (see note) |
| `ifar`          | `ifar`                |
| `H1_snr`        | `H1_snr`              |
| `L1_snr`        | `L1_snr`              |
| `V1_snr`        | NaN                   |
| `network_snr`   | sqrt(`H1_snr`² + `L1_snr`²) |
| `end_time_gps`  | `time` (see note)     |
| `p_astro`        | `pastro`              |
| `mass_1`         | `mass1`               |
| `mass_2`         | `mass2`               |
| `spin_1z`        | `spin1z`              |
| `spin_2z`        | `spin2z`              |


!!! question

    `obs` always equal to 'HLV', but on SNR for H1 and L1. Should check if Virgo has
    been used for establishing the trigger list.

!!! note

    The dataset keys follow the conventions of the 4-OGC data release,
    except that IAS trigger times are in 'linear-free' coordinates
    (note that these two conventions differ by a shift that depends on
    the intrinsic parameters, with a difference less than 0.1s for most sources
    but up to roughly 0.3s for the lightest BBH systems).
