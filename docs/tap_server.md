The IGWN TAP server is a deployment of the [DaCHS](http://docs.g-vo.org/DaCHS/) software stack.


## Table annotations for the VO tools

The Pipeline Trigger Table is annotated for interoperability with the VO tools.

* the units of the tables follow the IVOA Working Draft 2022-05-25 Units in the VO [Version 1.1](https://ivoa.net/documents/VOUnits/20220525/WD-VOUnits-1.1-20220525.pdf).

* the Unified Content Descriptions, which describe the physical meaning of the columns follow the IVOA Standard for Unified Content Descriptors
[Version 1.10](https://www.ivoa.net/documents/latest/UCD.html). The [IVAO UCD builder](http://cdsweb.u-strasbg.fr/UCD/cgi-bin/descr2ucd) was handy to find the UCD corresponding to a description in natural language.

!!! note
    No corresponding UCD has been found for the effective inspiral spin parameter. We are interacting with the IVOA to solve this issue.


## Column indexing

The table has an index for the following columns:

* `catalog`
* `end_time_gps`

As we gain understanding on how the Pipeline Trigger Database is used, we may index more columns.

## Deployment of new trigger release

Through ansible, TBD
