# -- GWTC 2.1 Candidate data release
set -ex

wget https://zenodo.org/record/5759108/files/search_data_GWTC2p1.tar.gz?download=1 -O data-gwtc2p1.tar.gz
tar -xf data-gwtc2p1.tar.gz
rm data-gwtc2p1.tar.gz

mkdir -p original_data
mv search_data_products/* original_data/
mv original_data/gstlal_all_sky original_data/GWTC2p1-GSTLAL_AllSky
mv original_data/mbta_all_sky original_data/GWTC2p1-MBTA_AllSky
mv original_data/pycbc_all_sky original_data/GWTC2p1-PYCBC_AllSky
mv original_data/pycbc_highmass original_data/GWTC2p1-PYCBC_HighMass
