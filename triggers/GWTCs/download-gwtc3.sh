# -- GWTC-3 Candidate data release
set -ex

wget https://zenodo.org/record/5546665/files/search_data.tar.gz?download=1 -O data-gwtc3.tar.gz
tar -xf data-gwtc3.tar.gz
rm data-gwtc3.tar.gz

mkdir -p original_data
mv search_data_products/* original_data/
mv original_data/cwb_allsky original_data/GWTC3-CWB_AllSky
mv original_data/gstlal_allsky original_data/GWTC3-GSTLAL_AllSky
mv original_data/mbta_all_sky original_data/GWTC3-MBTA_AllSky
mv original_data/pycbc_all_sky original_data/GWTC3-PYCBC_AllSky
mv original_data/pycbc_highmass original_data/GWTC3-PYCBC_HighMass
