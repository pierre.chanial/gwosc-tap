from pathlib import Path

from astropy.table import Table, vstack

# check that we can stack the standard tables
catalogs = Path(__file__).parent.glob('*/converted_data/*hdf5')
tables = [Table.read(catalog, path='standard') for catalog in catalogs]
table = vstack(tables)

catalogs = list(Path(__file__).parent.glob('GWTCs/converted_data/*hdf5'))
tables = [Table.read(catalog, path='coinc_inspiral') for catalog in catalogs]
coinc = vstack(tables)
tables = [Table.read(catalog, path='sngl_inspiral') for catalog in catalogs]
sngl = vstack(tables)
